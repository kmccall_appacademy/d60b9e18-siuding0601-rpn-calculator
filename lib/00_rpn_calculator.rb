require 'byebug'
class RPNCalculator

  def initialize
    @array = []
    @count = 0
  end

  def push(num)
    @array << num.to_f
    @count += 1
  end

  def value
    stack = []
    @array.each do |el|
      if el.class != String
        stack << el
      elsif el.class == String && stack.count < 2
        return "calculator is empty"
      elsif el == "+" && stack.count >= 2
        two_nums = stack.pop(2)
        stack << two_nums[0] + two_nums[1]
      elsif el == "-" && stack.count >= 2
        two_nums = stack.pop(2)
        stack << two_nums[0] - two_nums[1]
      elsif el == "*" && stack.count >= 2
        two_nums = stack.pop(2)
        stack << two_nums[0] * two_nums[1]
      elsif el == "/" && stack.count >= 2
        two_nums = stack.pop(2)
        stack << two_nums[0] / two_nums[1]
      end
    end
    stack[-1]
  end

  def minus
    if @array.count < 2
      raise "calculator is empty"
    end
    @array << "-"
  end

  def divide
    if @array.count < 2
      raise "calculator is empty"
    end
    @array << "/"
  end

  def times
    if @array.count < 2
      raise "calculator is empty"
    end
    @array << "*"
  end

  def plus
    if @array.count < 2
      raise "calculator is empty"
    end
    @array << "+"
  end

  def tokens(string_with_spaces)
    ans =[]
    string_with_spaces.split(" ").each do |el|
      if el.class == String && !"+-*/".include?(el)
        ans << el.to_i
      elsif "+-*/".include?(el)
        ans << el.to_sym
      end
    end
    ans
  end

  def evaluate(string_with_spaces)
    ans = tokens(string_with_spaces)
    stack = []
    ans.each do |el|
      if el.class == Fixnum
        stack << el.to_f
      elsif el == :+ && stack.count >= 2
        two_nums = stack.pop(2)
        stack << two_nums[0] + two_nums[1]
      elsif el == :- && stack.count >= 2
        two_nums = stack.pop(2)
        stack << two_nums[0] - two_nums[1]
      elsif el == :* && stack.count >= 2
        two_nums = stack.pop(2)
        stack << two_nums[0] * two_nums[1]
      elsif el == :/ && stack.count >= 2
        two_nums = stack.pop(2)
        stack << two_nums[0] / two_nums[1]
      end
    end
      stack[-1]
  end



end
